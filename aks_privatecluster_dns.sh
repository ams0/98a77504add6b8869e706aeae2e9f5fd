‎‎​#/bin/bash
#AKS private clusters

#Setup VPN gw with OpenVPN


rg=k8s
clustername=private

az aks get-credentials  -g $rg -n $clustername

fqdn=`az aks show -g $rg -n private --query privateFqdn -o tsv  2> /dev/null`
record=`az aks show -g $rg -n private --query privateFqdn -o tsv 2> /dev/null | sed 's/\..*$//'`
rg=`az aks show -g $rg -n private --query nodeResourceGroup -o tsv 2> /dev/null`
zone=`az aks show -g $rg -n private --query privateFqdn -o tsv 2> /dev/null | sed 's/^[^.]*.//g'`
address=`az network private-dns record-set a show -g $rg -z $zone -n $record -o tsv --query aRecords[0].ipv4Address`

#sed -i "s/$fqdn/$address/g" ~/.kube/config
echo "$address $fqdn" >> /etc/hostsa